<?php
/**
 * Plugin Name:     AGWEB Custom Code
 * Plugin URI:      PLUGIN SITE HERE
 * Description:     Custom website code.
 * Author:          Andre Gagnon
 * Author URI:      https://andregagnon.com
 * Text Domain:     agweb-custom-code
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Agweb_Custom_Code
 */

//////////////////////////////////
//
// WooCommerce
//

// Remove WooCommerce Updater
remove_action('admin_notices', 'woothemes_updater_notice');
// remove woocommerce nag message
add_filter( 'woocommerce_helper_suppress_admin_notices', '__return_true' );


// custom product page

function fun_wc_image_replacement( ) {
	// remove default action
	remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );

	if ( has_term( 'care-plan', 'product_cat', $post 	) == false) {
		woocommerce_show_product_images(); }
	else {

	global $post, $product;

	$columns           = apply_filters( 'woocommerce_product_thumbnails_columns', 4 );
	$thumbnail_size    = apply_filters( 'woocommerce_product_thumbnails_large_size', 'full' );
	$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
	$full_size_image   = wp_get_attachment_image_src( $post_thumbnail_id, $thumbnail_size );
	$placeholder       = has_post_thumbnail() ? 'with-images' : 'without-images';
	$wrapper_classes   = apply_filters( 'woocommerce_single_product_image_gallery_classes', array(
		'woocommerce-product-gallery',
		'woocommerce-product-gallery--' . $placeholder,
		'woocommerce-product-gallery--columns-' . absint( $columns ),
		'images',
	) );
	?>
	<div class="<?php echo esc_attr( implode( ' ', array_map( 'sanitize_html_class', $wrapper_classes ) ) ); ?>" data-columns="<?php echo esc_attr( $columns ); ?>" style="opacity: 0; transition: opacity .25s ease-in-out;">
		<br><h5>DESCRIPTION</h5>
		<h4><?php echo wpautop( $post->post_title); ?></h4>
		<p><?php echo wpautop( $post->post_content); ?></p>
	</div>

	<?php

	// remove extra information
	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
	}

	//var_dump( $product);
}
// add action to occur before default action, higher priority
add_action( 'woocommerce_before_single_product_summary', 'fun_wc_image_replacement', 10 );


//add_action( 'plugins_loaded', 'agweb_theme_customizations_main' );
add_action( 'init', 'agweb_theme_customizations_main' );

function agweb_theme_customizations_main() {

	// remove breadcrumbs
	remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
	// remove results text
	remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
	// remove sort order in products display
	remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
	// remove meta
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
}




//
// Custom URL field for website care plan product
//

/**
* Adds custom field for Product
* @return [type] [description]
*/
function fun_wc_add_custom_fields()
{

     global $product;

     ob_start();

     ?>
         <div class="wdm-custom-fields" style="margin-bottom: 20px;">
 						<label>URL:</label>
             <input type="text" name="wdm_name" required>
         </div>
         <div class="clear"></div>

     <?php

     $content = ob_get_contents();
     ob_end_flush();

     return $content;
}
// add a custom product fields to single product page
//add_action('woocommerce_before_add_to_cart_button','fun_wc_add_custom_fields');
